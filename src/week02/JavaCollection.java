package week02;
import java.util.*;

class Main {

    public static void main(String[] args) {
        System.out.println("--List--");
        List list = new ArrayList();
        list.add("");
        list.add("my");
        list.add("name");
        list.add("is");
        list.add("Cory");
        list.add("McGrew"+"\n");
        for (Object str : list) {
            System.out.println((String) str);
    }

        System.out.println("--Set(minecraft)--");
    Set set = new TreeSet();
        set.add("diamond Helmet");
        set.add("diamond Chestplate");
        set.add("diamond Leggings");
        set.add("diamond Boots");
        set.add("diamond Pickaxe");
        set.add("diamond Sword"+"\n");

        for (Object str : set) {
        System.out.println((String)str);
    }

        System.out.println("--Queue(For Cooking Bacon!!!)--");
    Queue queue = new PriorityQueue();
        queue.add("Step 7:   Eat the bacon!" +"\n");
        queue.add("Step 2:   Cook bacon low and slow");
        queue.add("Step 3b:  Take care of the grease burn you got");
        queue.add("Step 5:   Let cooked bacon drain");
        queue.add("Step 1:   Start with cold pan");
        queue.add("Step 3a:  Pour off grease carefully");
        queue.add("Step 6:   Cook remaining bacon in batches");
        queue.add("Step 4:   Cook bacon to desired doneness (crispy or chewy)");


        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
        System.out.println(queue.poll());
    }

        System.out.println("-- Map --");
    Map map = new HashMap();

        map.put(1,"From");
        map.put(2,"the");
        map.put(3,"Shire");
        map.put(4,"to");
        map.put(5,"Mordor"+"\n");


        for (int i = 1; i < 6; i++) {
        String result = (String)map.get(i);
        System.out.println(result);
}
      System.out.println("-- List using Films --");
    List<films> myList = new LinkedList<films>();
        myList.add(new films("Star Wars: Episode IV -  A New Hope", "George Lucas", "11 Million USD"));
        myList.add(new films("Jurassic Park", "Steven Spielberg", "63 Million USD" ));
        myList.add(new films("Avatar", "James Cameron", "237 Million USD" ));
        myList.add(new films("Finding Nemo", "Andrew Stanton", "94 Million USD"));

        for (films film : myList) {
        System.out.println(film);
        }
        }}