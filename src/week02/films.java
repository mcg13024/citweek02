package week02;

        public class films {

            private String title;
            private String director;
            private String budget;


            public films(String title, String director, String budget) {
                this.title = title;
                this.director = director;
                this.budget = budget;
            }

            public String toString() {
                return "Title: " + title + "\n"+ "Director: " + director +"\n"+ "Budget: " + budget+ "\n";
            }
        }